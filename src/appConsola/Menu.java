package appConsola;


import java.util.ArrayList;
import java.util.List;


/**
 * Clase que representa un menú de una aplicación. Un menú consiste en 1 o
 * varias {@link Entrada} que ejecutan una acción al ser elegidas, accedidas
 * con una cadena denominada opción. En un menú no puede haber dos entradas con
 * opciones iguales.
 *
 * @author LeoAM
 *
 */
public class Menu
{
    private List<Entrada> entradas = new ArrayList<Entrada>();
    private String        titulo   = "";

    /**
     * Crea un menú vacío.
     */
    public Menu()
    {
    }

    /**
     * Crea un menú sin entradas.
     *
     * @param titulo El título del menú.
     */
    public Menu(String titulo)
    {
        this.setTitulo(titulo);
    }

    /**
     * Regresa el título de este menú.
     *
     * @return El título de este menú
     */
    public String getTitulo()
    {
        return titulo;
    }

    /**
     * Fija el título de este menú.
     *
     * @param titulo El título a fijar.
     */
    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    /**
     * Agrega la entrada indicada al final de la lista de entradas de este
     * menú.
     *
     * @param entrada Entrada a agregar a este menú
     * @throws OpcionRepetidaExcepcion Si existe una entrada en el {@link Menu}
     * con la misma opción que esta entrada.
     */
    public void addEntrada(Entrada entrada)
        throws OpcionRepetidaExcepcion
    {
        if (entradas.contains(entrada))
        {
            throw new OpcionRepetidaExcepcion("Ya hay una entrada con esta opción.");
        }
        this.entradas.add(entrada);
    }

    /**
     * Regresa la entrada con la opción indicada.
     *
     * @param opcion Opción de la entrada a regresar.
     * @return La entrada con la opción indicada.
     * @throws OpcionInvalidaExcepcion Si no existe una entrada con la opción
     * indicada.
     */
    public Entrada getEntrada(String opcion)
        throws OpcionInvalidaExcepcion
    {
        this.entradas.get(0);
        for (Entrada entrada : entradas)
        {
            if (entrada.getOpcion().equals(opcion))
            {
                return entrada;
            }
        }
        throw new OpcionInvalidaExcepcion("Opción no encontrada");
    }

    /**
     * Quita la entrada en la posición indicada.
     *
     * @param indice Índice de la entrada a quitar.
     */
    public void removeEntrada(int indice)
    {
        this.entradas.remove(indice);
    }

    @Override
    public String toString()
    {
        List<String> listaEntradas = new ArrayList<String>();
        listaEntradas.add(this.titulo);
        for (Entrada entrada : this.entradas)
        {
            listaEntradas.add(entrada.toString());
        }
        return String.join("\n", listaEntradas);
    }
}
