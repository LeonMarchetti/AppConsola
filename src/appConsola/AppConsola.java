package appConsola;


import java.util.Scanner;


/**
 * Clase que representa una aplicación de consola. Se pueden ejecutar submenúes
 * al crear un objeto {@link Entrada} cuya acción sea crear una nueva
 * aplicación con otro objeto {@link Menu}.
 *
 * @author LeoAM
 *
 */
public class AppConsola
{
    private String         prompt  = "$ ";
    private static Scanner escaner = new Scanner(System.in);

    /**
     * Crea una aplicación de consola, con el prompt por defecto
     * "<code>$ </code>".
     */
    public AppConsola()
    {
    }

    /**
     * Crea una aplicación de consola, con el prompt indicado.
     *
     * @param prompt
     */
    public AppConsola(String prompt)
    {
        this.setPrompt(prompt);
    }

    /**
     * Regresa el prompt actual de la aplicación.
     *
     * @return El prompt
     */
    public String getPrompt()
    {
        return prompt;
    }

    /**
     * Fija el prompt de la aplicación al valor indicado.
     *
     * @param prompt El prompt a fijar.
     */
    public void setPrompt(String prompt)
    {
        this.prompt = prompt;
    }

    /**
     * Ejecuta el menú, mostrándolo por pantalla y aceptando entrada por
     * teclado.
     *
     * @param menu Menú a ejecutar
     */
    public void ejecutar(Menu menu)
    {
        while (true)
        {
            try
            {
                Entrada entrada;
                System.out.println(menu.toString());
                while (true)
                {
                    try
                    {
                        System.out.print(getPrompt());
                        String opcion = escaner.nextLine().trim();
                        entrada = menu.getEntrada(opcion);
                    }
                    catch (OpcionInvalidaExcepcion e)
                    {
                        // Opción inválida: Pregunto por entrada otra vez.
                        System.out.println(e.getMessage());
                        continue;
                    }
                    break;
                }
                entrada.ejecutar();
            }
            catch (SalirExcepcion e)
            {
                return;
            }
        }
    }
}
