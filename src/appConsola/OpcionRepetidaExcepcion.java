package appConsola;

public class OpcionRepetidaExcepcion extends Exception
{
    private static final long serialVersionUID = 1L;

    public OpcionRepetidaExcepcion()
    {
        super();
    }

    public OpcionRepetidaExcepcion(String mensaje)
    {
        super(mensaje);
    }
}
