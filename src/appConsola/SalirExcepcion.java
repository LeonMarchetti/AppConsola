package appConsola;


public class SalirExcepcion extends Exception
{
    private static final long serialVersionUID = 1L;

    public SalirExcepcion()
    {
    }

    public SalirExcepcion(String mensaje)
    {
        super(mensaje);
    }
}
