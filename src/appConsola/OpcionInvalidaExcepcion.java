package appConsola;


public class OpcionInvalidaExcepcion extends Exception
{
    private static final long serialVersionUID = 1L;

    public OpcionInvalidaExcepcion()
    {
        super();
    }

    public OpcionInvalidaExcepcion(String mensaje)
    {
        super(mensaje);
    }
}
