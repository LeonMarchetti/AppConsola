package appConsola;


/**
 * Clase que representa una entrada en un {@link Menu} de la aplicación. Cada
 * entrada tiene una opción, que es una cadena según la cual se puede acceder a
 * esta entrada y ejecutar una acción, y un texto descriptivo, y una acción a
 * ejecutar.
 *
 * @author LeoAM
 *
 */
public abstract class Entrada
{
    /**
     * Cadena que identifica a la entrada en el {@link Menu}, y por la cual se
     * ejecuta.
     */
    protected String opcion;
    /**
     * Texto descriptivo de la entrada.
     */
    protected String texto;

    /**
     * Crea una entrada vacía.
     */
    public Entrada()
    {
    }

    /**
     * Crea una entrada.
     *
     * @param opcion Opción de la entrada.
     * @param texto Texto de la entrada.
     */
    public Entrada(String opcion, String texto)
    {
        this.opcion = opcion;
        this.texto = texto;
    }

    /**
     * Regresa el texto de esta entrada.
     *
     * @return Texto de esta entrada.
     */
    public String getTexto()
    {
        return texto;
    }

    /**
     * Fija el texto de esta entrada.
     *
     * @param texto El texto a fijar.
     */
    public void setTexto(String texto)
    {
        this.texto = texto;
    }

    /**
     * Regresa la opción de esta entrada.
     *
     * @return Opción de esta entrada.
     */
    public String getOpcion()
    {
        return opcion;
    }

    /**
     * Fija la opción de esta entrada.
     *
     * @param opcion La opción a fijar.
     */
    public void setOpcion(String opcion)
    {
        this.opcion = opcion;
    }

    @Override
    public String toString()
    {
        return this.opcion + ". " + this.texto;
    }

    @Override
    public boolean equals(Object objeto)
    {
        if (objeto == null)
        {
            return false;
        }
        if (objeto == this)
        {
            return true;
        }
        if (!(objeto instanceof Entrada))
        {
            return false;
        }
        Entrada entrada = (Entrada) objeto;
        return this.opcion.equals(entrada.opcion);
    }

    @Override
    public int hashCode()
    {
        return this.opcion.hashCode();
    }

    /**
     * Acción a ejecutar cuando se elige esta entrada. Se puede lanzar la
     * excepción {@link SalirExcepcion} para cortar la ejecución del menú de
     * esta entrada.
     *
     * @throws SalirExcepcion Excepción que corta la ejecución del {@link Menu}
     * al que pertenece esta entrada.
     */
    protected abstract void accion() throws SalirExcepcion;

    /**
     * Ejecuta la acción de esta entrada.
     *
     * @throws SalirExcepcion Si la acción de esta entrada busca terminar la
     * ejecución del {@link Menu} de esta entrada.
     */
    public void ejecutar()
        throws SalirExcepcion
    {
        accion();
    }

    /**
     * Lanza una excepción para salir del {@link Menu} de esta entrada.
     *
     * @throws SalirExcepcion
     */
    protected void salir()
        throws SalirExcepcion
    {
        throw new SalirExcepcion();
    }
}
